'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _rc = require('rc');

var _rc2 = _interopRequireDefault(_rc);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const config = (0, _rc2.default)('common', {
  db: {
    uri: 'mongodb://localhost/resizer'
  },
  saltRounds: 10,
  jwtSecret: 'jwtSecret'
});

exports.default = config;
//# sourceMappingURL=config.js.map