'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const bucket = new _mongoose2.default.mongo.GridFSBucket(_mongoose2.default.connection.db, {
  bucketName: 'uploads'
});

exports.default = bucket;
//# sourceMappingURL=Upload.js.map