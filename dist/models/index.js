'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

require('./connect');

var _User = require('./User');

var _User2 = _interopRequireDefault(_User);

var _Upload = require('./Upload');

var _Upload2 = _interopRequireDefault(_Upload);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const models = {
  User: _User2.default,
  Upload: _Upload2.default
};

exports.default = models;
//# sourceMappingURL=index.js.map