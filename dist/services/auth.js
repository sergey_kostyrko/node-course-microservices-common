'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hashPassword = hashPassword;
exports.getToken = getToken;
exports.verifyToken = verifyToken;
exports.verifyPassword = verifyPassword;

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _config = require('../config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

async function hashPassword(password) {
  const hash = _bcrypt2.default.hash(password, _config2.default.saltRounds);

  return hash;
}

function getToken(data) {
  return _jsonwebtoken2.default.sign(data, _config2.default.jwtSecret);
}

async function verifyToken(token) {
  const decoded = await _jsonwebtoken2.default.verify(token, _config2.default.jwtSecret);

  return decoded;
}

async function verifyPassword(password, hash) {
  const valid = await _bcrypt2.default.compare(password, hash);

  return valid;
}
//# sourceMappingURL=auth.js.map