'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createUpload = createUpload;
exports.getList = getList;
exports.getUpdatedList = getUpdatedList;
exports.getUploadStream = getUploadStream;
exports.deleteUpload = deleteUpload;
exports.saveUpload = saveUpload;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _models = require('../models');

var _models2 = _interopRequireDefault(_models);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const { Upload } = _models2.default;

async function createUpload(data) {
  return new Promise(resolve => {
    const stream = Upload.openUploadStream(data.filename, {
      contentType: data.contentType,
      metadata: data.metadata
    });
    stream.write('');
    stream.end();
    stream.on('finish', () => {
      resolve({ id: stream.id });
    });
  });
}

async function getList(userId) {
  const cursor = await Upload.find({
    'metadata.userId': userId
  });
  const list = await cursor.toArray();

  return list;
}

async function getUpdatedList(userId, timestamp) {
  const cursor = await Upload.find({
    'metadata.userId': userId,
    'metadata.updated': { $gt: timestamp }
  });
  const list = await cursor.toArray();

  return list;
}

async function getUploadStream(id) {
  const objectId = _mongoose2.default.Types.ObjectId(id);
  return Upload.openDownloadStream(objectId);
}

async function deleteUpload(id) {
  const objectId = _mongoose2.default.Types.ObjectId(id);
  return Upload.delete(objectId);
}

async function saveUpload(data, next) {
  const { id, contentType, filename, content } = data;
  const objectId = _mongoose2.default.Types.ObjectId(id);
  await Upload.delete(objectId);
  delete data.content;
  const stream = Upload.openUploadStreamWithId(objectId, filename, {
    contentType,
    metadata: Object.assign({}, data.metadata, { processed: true })
  });
  stream.write(content);
  stream.end();
  stream.on('finish', () => {
    next();
  });
}
//# sourceMappingURL=uploads.js.map