'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllUsers = getAllUsers;
exports.getUserById = getUserById;
exports.findUser = findUser;
exports.createUser = createUser;
exports.updateUser = updateUser;

var _auth = require('./auth');

var _models = require('../models');

var _models2 = _interopRequireDefault(_models);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

async function getAllUsers() {
  const users = await _models2.default.User.find({});
  return users;
}

async function getUserById(id) {
  const user = await _models2.default.User.findById(id);
  return user;
}

async function findUser(where) {
  const user = await _models2.default.User.findOne(where);
  return user;
}

async function createUser(data) {
  if ('password' in data) {
    data.password = await (0, _auth.hashPassword)(data.password);
  }
  await _models2.default.User.create(data);
}

async function updateUser(user, data) {
  if ('password' in data) {
    data.password = await (0, _auth.hashPassword)(data.password);
    data.acceptTokensSince = Date.now();
  }

  await user.update(data);
}
//# sourceMappingURL=users.js.map