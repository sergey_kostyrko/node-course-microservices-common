import rc from 'rc';

const config = rc('common', {
  db: {
    uri: 'mongodb://localhost/resizer',
  },
  saltRounds: 10,
  jwtSecret: 'jwtSecret',
});

export default config;
