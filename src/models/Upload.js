import mongoose from 'mongoose';

const bucket = new mongoose.mongo.GridFSBucket(mongoose.connection.db, {
  bucketName: 'uploads',
});

export default bucket;
