import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const schema = new Schema({
  login: { type: String, index: { unique: true } },
  password: String,
  email: { type: String, index: { unique: true } },
});

export default mongoose.model('User', schema);
