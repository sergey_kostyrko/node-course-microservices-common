import './connect';
import User from './User';
import Upload from './Upload';

const models = {
  User,
  Upload,
};

export default models;
