import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import config from '../config';

export async function hashPassword(password) {
  const hash = bcrypt.hash(password, config.saltRounds);

  return hash;
}

export function getToken(data) {
  return jwt.sign(data, config.jwtSecret);
}

export async function verifyToken(token) {
  const decoded = await jwt.verify(token, config.jwtSecret);

  return decoded;
}

export async function verifyPassword(password, hash) {
  const valid = await bcrypt.compare(password, hash);

  return valid;
}
