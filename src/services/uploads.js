import mongoose from 'mongoose';
import models from '../models';

const { Upload } = models;

export async function createUpload(data) {
  return new Promise(resolve => {
    const stream = Upload.openUploadStream(data.filename, {
      contentType: data.contentType,
      metadata: data.metadata,
    });
    stream.write('');
    stream.end();
    stream.on('finish', () => {
      resolve({ id: stream.id });
    });
  });
}

export async function getList(userId) {
  const cursor = await Upload.find({
    'metadata.userId': userId,
  });
  const list = await cursor.toArray();

  return list;
}

export async function getUpdatedList(userId, timestamp) {
  const cursor = await Upload.find({
    'metadata.userId': userId,
    'metadata.updated': { $gt: timestamp },
  });
  const list = await cursor.toArray();

  return list;
}

export async function getUploadStream(id) {
  const objectId = mongoose.Types.ObjectId(id);
  return Upload.openDownloadStream(objectId);
}

export async function deleteUpload(id) {
  const objectId = mongoose.Types.ObjectId(id);
  return Upload.delete(objectId);
}

export async function saveUpload(data, next) {
  const { id, contentType, filename, content } = data;
  const objectId = mongoose.Types.ObjectId(id);
  await Upload.delete(objectId);
  delete data.content;
  const stream = Upload.openUploadStreamWithId(objectId, filename, {
    contentType,
    metadata: Object.assign({}, data.metadata, { processed: true }),
  });
  stream.write(content);
  stream.end();
  stream.on('finish', () => {
    next();
  });
}
