import { hashPassword } from './auth';
import db from '../models';

export async function getAllUsers() {
  const users = await db.User.find({});
  return users;
}

export async function getUserById(id) {
  const user = await db.User.findById(id);
  return user;
}

export async function findUser(where) {
  const user = await db.User.findOne(where);
  return user;
}

export async function createUser(data) {
  if ('password' in data) {
    data.password = await hashPassword(data.password);
  }
  await db.User.create(data);
}

export async function updateUser(user, data) {
  if ('password' in data) {
    data.password = await hashPassword(data.password);
    data.acceptTokensSince = Date.now();
  }

  await user.update(data);
}
